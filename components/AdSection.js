import React from 'react';
import ImageView from "./common/imageView";

const AdSection = () => {
    return (
        <div className='flex justify-center mt-20 mb-20'>
            <img src="/images/Offer-Banner.jpg" alt=""/>
        </div>
    );
};

export default AdSection;
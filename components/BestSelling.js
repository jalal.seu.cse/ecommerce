import React from 'react';
import {Swiper, SwiperSlide} from "swiper/react";
import {A11y, Navigation} from "swiper";
import Product from "./common/Product";

const BestSelling = () => {
    const product = [
        {
            id: 1,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product1.jpg',
            price: 122,
        },
        {
            id: 2,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product2.jpg',
            price: 250,
        },
        {
            id: 3,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product3.jpg',
            price: 460,
        },
        {
            id: 4,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product4.jpg',
            price: 2500,
        },
        {
            id: 6,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product5.jpg',
            price: 960,
        },
        {
            id: 7,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product6.jpg',
            price: 122,
        },
        {
            id: 8,
            name: 'Comfortable Anti Pollution Multi Layers',
            image: '/images/product7.jpg',
            price: 122,
        },
    ]

    return (
        <div className='sm:pl-44 sm:pr-44 mb-20 pl-10 pr-10 mt-28 mb-24'>
            <div className='flex flex-col items-center'>
                <p className='text-gray-600'>You,re looking for</p>
                <h1 className='heading'>Best Selling Items</h1>
            </div>

            <Swiper
                // install Swiper modules
                modules={[
                    Navigation,
                    // Pagination,
                    // Scrollbar,
                    A11y
                ]}
                spaceBetween={20}
                // slidesPerView={5}
                breakpoints={{
                    200: {
                        slidesPerView: 2,
                    },
                    768: {
                        slidesPerView: 5,
                    },
                }}
                navigation
                pagination={{ clickable: true }}
                // scrollbar={{ draggable: true }}
                onSwiper={(swiper) => console.log(swiper)}
                onSlideChange={() => console.log('slide change')}
            >
                {
                    product.map(data =>
                        <SwiperSlide>
                            <Product
                                url={data?.image}
                                name={data?.name}
                                price={data?.price}
                            />

                        </SwiperSlide>
                    )
                }

            </Swiper>

        </div>
    );
};

export default BestSelling;
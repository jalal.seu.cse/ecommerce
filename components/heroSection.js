import React from 'react';
import Image from "next/image";

const HeroSection = () => {
    return (
        // <div>
            <div className='relative w-full h-30'>

                <img src="/images/Main-Banner-1-1903x680.jpg" alt=""/>

                <Image
                    src='/images/Main-Banner-1-1903x680.jpg'
                    width="100%"
                    height="100%"
                    objectFit="cover"
                    layout="fill"
                    alt=""
                />
            </div>
        // </div>
    );
};

export default HeroSection;
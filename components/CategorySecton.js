import React from 'react';
import Link from "next/link";

const CategorySecton = () => {
    return (
            <div className="grid grid-cols-1 sm:grid-cols-3 gap-x-16 small-screen sm:md-screen mt-20 mb-10">

                <div className='w-full grid-cols-1 h-96 bg-no-repeat bg-center pt-40 md:pt-7 pl-7 bg-contain'
                     style={{
                         backgroundImage: "url(" + "/images/Category-banner1.jpg" + ")"
                     }}
                >
                    <h2 className='text-gray-600'>Designer</h2>
                    <h1 className='pt-4 pb-2 text-3xl font-medium'>Flooring <br/> Light</h1>
                    <p className='text-primary underline underline-offset-1'>Shop Now</p>
                </div>

                <div className='w-full grid-cols-1 h-96 bg-contain bg-no-repeat bg-center pt-40 md:pt-7 pl-7'
                     style={{
                         backgroundImage: "url(" + "/images/Category-banner2.jpg" + ")"
                     }}
                >
                    <h2 className='text-gray-600'>Designer</h2>
                    <h1 className='pt-4 pb-2 text-3xl font-medium'>Flooring <br/> Light</h1>
                    <p className='text-primary underline underline-offset-1'>Shop Now</p>
                </div>

                <div className='w-full grid-cols-1 h-96 bg-contain bg-no-repeat bg-center pt-40 md:pt-7 pl-7'
                     style={{
                         backgroundImage: "url(" + "/images/Category-banner3.jpg" + ")"
                     }}
                >
                    <h2 className='text-gray-600'>Designer</h2>
                    <h1 className='pt-4 pb-2 text-3xl font-medium'>Flooring <br/> Light</h1>
                    <p className='text-primary underline underline-offset-1'>Shop Now</p>
                </div>

            </div>
    );
};

export default CategorySecton;
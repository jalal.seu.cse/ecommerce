import React from 'react';

const CardSection2 = () => {
    return (
        <div className="grid grid-cols-1 sm:grid-cols-2 gap-x-16 small-screen sm:md-screen md:mt-20 md:mb-10">

            <div className='w-full grid-cols-1 h-96 bg-no-repeat bg-center pl-14 bg-contain flex flex-col justify-center'
                 style={{
                     backgroundImage: "url(" + "/images/Cms-Banner-6.jpg" + ")"
                 }}
            >
                <h2 className='text-gray-600'>Designer</h2>
                <h1 className='pt-4 pb-2 text-3xl font-medium'>Flooring <br/> Light</h1>
                <p className='text-primary underline underline-offset-1'>Shop Now</p>
            </div>

            <div className='w-full grid-cols-1 h-96 bg-no-repeat bg-center pl-14 bg-contain flex flex-col justify-center'

                 style={{
                     backgroundImage: "url(" + "/images/Cms-Banner-7.jpg" + ")"
                 }}
            >
                <h2 className='text-gray-600'>Designer</h2>
                <h1 className='pt-4 pb-2 text-3xl font-medium'>Flooring <br/> Light</h1>
                <p className='text-primary underline underline-offset-1'>Shop Now</p>
            </div>

        </div>
    );
};

export default CardSection2;
import React from 'react';
import ImageView from "./common/imageView";

const TopNavbar = () => {
    return (
        <div>
            <nav className='pl-7 pr-7 pt-4 pb-2 flex justify-between'>
                <div className='flex justify-between space-x-8 '>


                       <div className='relative w-44 h-30'>
                           <ImageView
                               url='/images/Logo.png'
                           />
                       </div>

                    <div className="space-y-1 mt-3">
                        <div className="w-6 h-1 bg-gray-600"></div>
                        <div className="w-6 h-1 bg-gray-600"></div>
                        <div className="w-6 h-1 bg-gray-600"></div>
                    </div>

                    <div>
                        <div className="relative xl:block hidden">
                            <input
                                className="md:w-128 w-36 px-8 py-3 border lg:rounded-full shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline bg-gray-100"
                                id="username" type="text" placeholder="Search product"/>
                            <div className="flex absolute inset-y-0 right-0 items-center pl-3 mr-8">
                                <img className='w-4 h-4 mt-1'
                                     src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/search_black.svg"
                                     alt="search"/>
                            </div>

                        </div>
                    </div>


                    {/*<div className='flex'>*/}
                    {/*    <input style={{width: '720px'}} className="border rounded-3xl shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="username" type="text" placeholder="Username"/>*/}
                    {/*    <img className='w-8 h-8 mt-1' src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/search_black.svg" alt="search"/>*/}
                    {/*</div>*/}


                </div>
                <div className='flex items-center pr-7 inline-block align-middle space-x-5'>
                    <div className='xl:block hidden'>
                        <button id="dropdownDefault" data-dropdown-toggle="dropdown"
                                className="text-white bg-primary focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-4 py-2.5 text-center inline-flex items-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                type="button">My Account <svg className="ml-2 w-4 h-4" aria-hidden="true" fill="none"
                                                                   stroke="currentColor" viewBox="0 0 24 24"
                                                                   xmlns="http://www.w3.org/2000/svg">
                            <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="M19 9l-7 7-7-7"></path>
                        </svg></button>
                    </div>
                    <div>
                        <img className='w-5' src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/compare.svg" alt="image"/>
                    </div>
                    <div>
                        <img className='w-5' src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/Wishlist.svg" alt="image"/>
                    </div>
                    <div>
                        <img className='w-5' src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/bag.svg" alt="image"/>
                    </div>
                </div>

            </nav>
            <div className="divide-y">
                <div></div>
                <div></div>
            </div>
        </div>
    );
};

export default TopNavbar;
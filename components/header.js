import React from 'react';

const Header = () => {
    return (
        <div className='flex pl-80 pt-2 pb-2 space-x-12 xs:hidden hidden sm:block'>
            <button type="button"
            >
                Home
            </button>
            <button type="button"
            >
                Fashion
            </button>
            <button type="button"
            >
                Gadgets
            </button>
            <button type="button"
            >
                Electronics
            </button>
            <button type="button"
            >
                Applicatiences
            </button>
            <button type="button"
            >
                AutoParts
            </button>
            <button type="button"
            >
                kids Toys
            </button>
        </div>
    );
};

export default Header;
import React from 'react';
import Image from "next/image";

const ImageView = ({url}) => {

    return (
            <Image
                src={url}
                width="100%"
                height="100%"
                objectFit="cover"
                layout="fill"
                alt=""
            />
    );
};

export default ImageView;
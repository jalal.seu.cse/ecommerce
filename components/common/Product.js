import React from 'react';
import ImageView from "./imageView";

const Product = ({url, name, price}) => {
    return (
        <div className='mt-20 '>
            <div className='relative w-full h-52 mb-14'>
                <ImageView
                    url={url}
                />
            </div>
            <h1 className='text-gray-800'>{name}</h1>
            <p className='font-medium mt-2'>${price}</p>
        </div>
    );
};

export default Product;
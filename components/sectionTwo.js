import React from 'react';

const SectionTwo = () => {
    return (
        <div>
            <div className="grid grid-cols-1 sm:grid-cols-3 gap-x-16 small-screen sm:md-screen mt-20 mb-10">
                <div className='grid grid-cols-1 sm:grid-cols-4 gap-x-4 mb-10'>
                    <div className='flex justify-center'>
                        <img
                            className='h-12'
                            src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/truck.svg"
                             alt="icon"
                        />
                    </div>
                    <div className='sm:col-span-3 flex flex-col'>
                        <h1 className='text-black font-medium text-xl text-center sm:text-left'>Online 24/7 Supports</h1>
                        <p className='text-gray-600 text-center sm: md:text-left'>Contrary popular belief Lorem Ipsu</p>
                    </div>
                </div>


                <div className='grid grid-cols-1 sm:grid-cols-4 gap-x-4 mb-10'>
                    <div className='flex justify-center'>
                        <img
                            className='h-12'
                            src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/support.svg"
                            alt="icon"
                        />
                    </div>
                    <div className='sm:col-span-3 flex flex-col'>
                        <h1 className='text-black font-medium text-xl text-center sm:text-left'>Fast Free Delivery</h1>
                        <p className='text-gray-600 text-center sm: md:text-left'>Contrary popular belief Lorem Ipsu</p>
                    </div>
                </div>


                <div className='grid grid-cols-1 sm:grid-cols-4 gap-x-4 mb-10'>
                    <div className='flex justify-center'>
                        <img
                            className='h-12'
                            src="https://opencart.templatemela.com/OPC10/OPC100247/OPC5/catalog/view/theme/OPC100247_5/image/megnor/payment.svg"
                            alt="icon"
                        />
                    </div>
                    <div className='sm:col-span-3 flex flex-col'>
                        <h1 className='text-black font-medium text-xl text-center sm:text-left'>Fast Free Delivery</h1>
                        <p className='text-gray-600 text-center sm: md:text-left'>Contrary popular belief Lorem Ipsu</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default SectionTwo;
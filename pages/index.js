import TopNavbar from "../components/top-navbar";
import React from "react";
import Header from "../components/header";
import HeroSection from "../components/heroSection";
import SectionTwo from "../components/sectionTwo";
import CategorySecton from "../components/CategorySecton";
import TrendingSection from "../components/TrendingSection";
import AdSection from "../components/AdSection";
import BestSelling from "../components/BestSelling";
import CardSection2 from "../components/CardSection2";

export default function Home() {
  return (
   <div>
       <TopNavbar />
       <Header />
       <HeroSection />
       <SectionTwo />
       <CategorySecton />
       <TrendingSection />
       <AdSection />
       <BestSelling />
       <CardSection2 />
   </div>
  )
}
